using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using aspTest.Data;
using aspTest.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace appTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly DataContext _context;

        public ValuesController(DataContext context)
        {
            _context = context;
        }
        
        // GET api/values
        [HttpGet]
        public async Task<IActionResult> GetValues()
        {
            var values = await _context.Students.ToListAsync();

            return Ok(values);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetValue(int id)
        {
            var value = await _context.Students.FirstOrDefaultAsync(x => x.Id==id);
            
            return Ok(value);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> PostValue(Student student)
        {
            _context.Students.Add(student);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetValue), new { id = student.Id }, student);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutValue(int id, Student student)
        {
            if ( id != student.Id)
            {
                return BadRequest();
            }

            _context.Entry(student).State = EntityState.Modified;

            await _context.SaveChangesAsync();
            return NoContent();
           
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Student>> DeleteValue(int id)
        {
            var student = await _context.Students.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            _context.Students.Remove(student);
            await _context.SaveChangesAsync();

            return student;
        }
    }
}
